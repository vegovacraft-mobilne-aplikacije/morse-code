import 'package:flutter/material.dart';
import 'package:string_to_morse/screens/list.screen.dart';
import 'package:string_to_morse/screens/play.screen.dart';
import 'package:torch_controller/torch_controller.dart';
import './utils/morse.util.dart';

void main() {
  TorchController().initialize();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => const ListScreen(),
        '/play': (context) => const PlayScreen(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final TextEditingController _text = TextEditingController();
  final torchController = TorchController();

  String _morse = '';
  bool _on = false, _torchOn = false;

  void _turnOn() async {
    setState(() => _on = true);
    if (!_torchOn) {
      try {
        _torchOn = await torchController.toggle() ?? true;
      } catch (e) {
        print('No torch');
      }
    }
  }

  void _turnOff() async {
    setState(() => _on = false);
    if (_torchOn) {
      try {
        _torchOn = await torchController.toggle() ?? false;
      } catch (e) {
        print('No torch');
      }
    }
  }

  void _play() async {
    final String encoded = Morse.encode(_text.text);

    final chars = encoded.characters;

    for (final char in chars) {
      if (char == ' ') {
        await Future.delayed(const Duration(milliseconds: 1000));
        continue;
      }
      _turnOn();
      if (char == '.') {
        await Future.delayed(const Duration(milliseconds: 100));
      } else if (char == '-') {
        await Future.delayed(const Duration(milliseconds: 500));
      }
      _turnOff();
      await Future.delayed(const Duration(milliseconds: 500));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Spacer(flex: 1),
            Container(
              color: _on ? Colors.red : Colors.white,
              width: 200.0,
              height: 200.0,
            ),
            const SizedBox(height: 50.0),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: TextField(
                controller: _text,
                decoration: InputDecoration(
                  hintText: 'Input string',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  onPressed: () {
                    String encoded = Morse.encode(_text.text);
                    print(encoded);

                    setState(() {
                      _morse = encoded;
                    });
                  },
                  child: const Text('Encode'),
                ),
                const SizedBox(width: 10.0),
                ElevatedButton(
                  onPressed: _play,
                  child: const Text('Play'),
                ),
              ],
            ),
            const Text(
              'Morse:',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 24,
              ),
            ),
            Text(
              _morse,
              style: const TextStyle(
                fontSize: 28,
              ),
            ),
            const Spacer(flex: 2),
          ],
        ),
      ),
    );
  }
}
