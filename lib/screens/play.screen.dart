import 'package:flutter/material.dart';
import 'package:string_to_morse/utils/morse.util.dart';
import 'package:torch_controller/torch_controller.dart';

class PlayScreen extends StatefulWidget {
  const PlayScreen({Key? key}) : super(key: key);

  @override
  _PlayScreenState createState() => _PlayScreenState();
}

class _PlayScreenState extends State<PlayScreen> {
  final torchController = TorchController();
  bool _on = true, _torchOn = false, _started = false;

  void _turnOn() async {
    setState(() => _on = true);
    if (!_torchOn) {
      try {
        _torchOn = await torchController.toggle() ?? true;
      } catch (e) {
        print('No torch');
      }
    }
  }

  void _turnOff() async {
    setState(() => _on = false);
    if (_torchOn) {
      try {
        _torchOn = await torchController.toggle() ?? false;
      } catch (e) {
        print('No torch');
      }
    }
  }

  Future<void> _wait(int ms) => Future.delayed(Duration(milliseconds: ms));

  void _play(String text) async {
    _started = true;
    final String encoded = Morse.encode(text);

    final chars = encoded.characters;

    for (final char in chars) {
      if (char == ' ') {
        await _wait(1000);
        continue;
      }
      _turnOn();
      if (char == '.') {
        await _wait(200);
      } else if (char == '-') {
        await _wait(500);
      }
      _turnOff();
      await _wait(500);
    }

    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    if (!_started) {
      final String? text = ModalRoute.of(context)!.settings.arguments as String?;
      if (text != null) {
        _play(text);
      }
    }

    return Scaffold(
      backgroundColor: _on ? Colors.red : Colors.white,
    );
  }
}
