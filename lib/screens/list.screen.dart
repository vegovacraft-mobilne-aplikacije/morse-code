import 'package:flutter/material.dart';
import 'package:string_to_morse/widgets/add.widget.dart';
import 'package:string_to_morse/widgets/list.widget.dart';

class ListScreen extends StatefulWidget {
  const ListScreen({Key? key}) : super(key: key);

  @override
  State<ListScreen> createState() => _ListScreenState();
}

class _ListScreenState extends State<ListScreen> {
  final List<String> strings = [];

  void _onAdd(String text) {
    setState(() => strings.add(text));
  }

  void _onPlay(String text) {
    Navigator.of(context).pushNamed(
      '/play',
      arguments: text,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Morse Code'),
      ),
      body: Column(
        children: [
          AddWidget(
            onAdd: _onAdd,
          ),
          ListWidget(
            strings: strings.reversed.toList(),
            onPlay: _onPlay,
          ),
        ],
      ),
    );
  }
}
