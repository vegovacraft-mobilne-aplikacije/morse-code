import 'package:flutter/material.dart';

class ListWidget extends StatelessWidget {
  const ListWidget({
    Key? key,
    required this.strings,
    required this.onPlay,
  }) : super(key: key);

  final List<String> strings;
  final Function(String) onPlay;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: strings.length,
      itemBuilder: (BuildContext context, int i) => ListTile(
        title: Text(strings[i]),
        trailing: GestureDetector(
          onTap: () => onPlay(strings[i]),
          child: const Icon(Icons.play_arrow),
        ),
      ),
    );
  }
}
