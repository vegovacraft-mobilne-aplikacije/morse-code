import 'package:flutter/material.dart';

class AddWidget extends StatelessWidget {
  const AddWidget({
    Key? key,
    required this.onAdd,
  }) : super(key: key);

  final Function(String) onAdd;

  @override
  Widget build(BuildContext context) {
    final TextEditingController _controller = TextEditingController();

    void _onPressed() {
      if (_controller.text.isNotEmpty) {
        onAdd(_controller.text);
      }
    }

    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              controller: _controller,
            ),
          ),
          const SizedBox(width: 20.0),
          FloatingActionButton(
            mini: true,
            onPressed: _onPressed,
            child: const Icon(Icons.add),
          ),
        ],
      ),
    );
  }
}
